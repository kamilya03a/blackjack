"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.resetSum = exports.printWinnerComputersPlayers = exports.printWinnerPlayers = exports.CheckAnswer = exports.CheckCardsEnough = exports.getOut = exports.buildComputerPlayers = exports.CopyPlayers = exports.buildPlayers = void 0;
const player_1 = require("../classes/player");
const ReadLineSync = __importStar(require("readline-sync"));
const random_1 = require("./random");
function buildPlayers() {
    let players = ReadLineSync.questionInt("how many players do you want to game with?");
    if (players == 0)
        players = 1;
    let array = [];
    for (let i = 0; i < players; i++) {
        array.push(new player_1.Player(i));
    }
    return (array);
}
exports.buildPlayers = buildPlayers;
function CopyPlayers(players) {
    let tempPlayers = [];
    for (let i = 0; i < players.length; i++)
        tempPlayers.push(players[i]);
    return tempPlayers;
}
exports.CopyPlayers = CopyPlayers;
function buildComputerPlayers(randomNumOfRound, _max) {
    let players = ReadLineSync.questionInt("how many players do you want to game with?");
    let array = [];
    for (let i = 0; i < players - 1; i++) {
        array[i] = 0;
        array[i] = (0, random_1.randomNum)((0, random_1.randomNum)(randomNumOfRound));
    }
    return (array);
}
exports.buildComputerPlayers = buildComputerPlayers;
function getOut(arr, player) {
    let index = arr.indexOf(player);
    if (index !== -1)
        arr.splice(index, 1);
    return arr;
}
exports.getOut = getOut;
function CheckCardsEnough(numOfPlayers, deck) {
    return (deck >= numOfPlayers);
}
exports.CheckCardsEnough = CheckCardsEnough;
function CheckAnswer(question) {
    if (ReadLineSync.keyInYN(question)) {
        return true;
    }
    return false;
}
exports.CheckAnswer = CheckAnswer;
function printWinnerPlayers(players) {
    players.sort(function (a, b) {
        return (b.winner - a.winner);
    });
    console.log("The player winners in order are");
    for (let i = 0; i < players.length; i++) {
        console.log("player number" + players[i].getPlayerId() + "with " + players[i].winner + " winning");
    }
}
exports.printWinnerPlayers = printWinnerPlayers;
function printWinnerComputersPlayers(computerPlayers) {
    computerPlayers.sort(function (a, b) {
        return (b - a);
    });
    console.log("The player winners in order are");
    for (let i = 0; i < computerPlayers.length; i++) {
        console.log("player number " + i + "with " + computerPlayers[i] + " winning");
    }
}
exports.printWinnerComputersPlayers = printWinnerComputersPlayers;
function resetSum(array) {
    for (let index = 0; index < array.length; index++)
        array[index].setSum(0);
}
exports.resetSum = resetSum;
//# sourceMappingURL=functionHelpers.js.map
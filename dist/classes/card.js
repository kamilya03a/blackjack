"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Card = exports.Shape = void 0;
var Shape;
(function (Shape) {
    Shape[Shape["spade"] = 0] = "spade";
    Shape[Shape["heart"] = 1] = "heart";
    Shape[Shape["diamond"] = 2] = "diamond";
    Shape[Shape["club"] = 3] = "club";
})(Shape = exports.Shape || (exports.Shape = {}));
class Card {
    constructor(numberCard, shape) {
        this.numberCard = numberCard;
        this.shape = shape;
    }
    getValue() {
        return (this.numberCard);
    }
    GetShape() {
        return (this.shape);
    }
    ToString() {
        return ("the Number is " + this.numberCard + " \n his shape is " + this.shape);
    }
}
exports.Card = Card;
//# sourceMappingURL=card.js.map
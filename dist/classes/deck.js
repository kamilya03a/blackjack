"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Deck = void 0;
const random_1 = require("../helper/random");
const card_1 = require("./card");
const card_2 = require("./card");
class Deck {
    constructor() {
        this.size = 52;
        this.cards = new Array(this.size);
        this.lastCard = 0;
        Object.keys(card_2.Shape).forEach(shape => {
            for (let j = 1; j < 14; j++) {
                if (j > 10) {
                    const card = new card_1.Card(10, shape);
                    this.add(card);
                }
                else {
                    const card = new card_1.Card(j, shape);
                    this.add(card);
                }
            }
        });
        this.cards = (0, random_1.ShuffleDesk)(this.cards);
    }
    isEmpty() {
        if (this.lastCard > 0)
            return true;
        return false;
    }
    add(card) {
        this.cards[this.lastCard] = (card);
        this.lastCard++;
    }
    getCard() {
        //בהנחה שיש קלפים בחפיסה
        let tempCard = this.cards[this.lastCard - 1];
        this.lastCard--;
        return (tempCard);
    }
    getAmountOfCards() {
        return (this.size);
    }
    ToString() {
        let print = "";
        for (let i = 0; i < this.cards.length; i++) {
            print += this.cards[i].ToString();
        }
        return (print);
    }
}
exports.Deck = Deck;
//# sourceMappingURL=deck.js.map
import { Card } from "./card";
import * as ReadLineSync from "readline-sync";

export class Player {
 
    public winner : number;
    public sum: number;
    public cards: Card[];
    public lastIndex :number;
    public playerId:number
    
    constructor(index: number){
        this.winner = 0;
        this.sum = 0;
        this.lastIndex = 0;
        this.cards = [];
        this.playerId= index;
    }

    public setSum(newSum: number)
    {
        this.sum= newSum;      
    }

    public add(card: Card): void {
        this.cards[this.lastIndex] = card;
        this.lastIndex++;
        if(card.getValue()==1)
        {
            this.asCardValue(card)
        }
        else
            this.sum+= card.getValue();
    }
    public GetWinner():number{
        return (this.winner)
    }   
    public getSum(): number{
        return(this.sum);
    }
    public getPlayerId(): number{
        return(this.playerId);
    }
    private asCardValue(card: Card): void
    {
        if(!ReadLineSync.keyInYN("do you want to change the card to 11?"))
        {
            this.sum+= card.getValue();
        }
        else
            this.sum+= 11;
    }
}
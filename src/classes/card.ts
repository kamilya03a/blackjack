export enum Shape{
    spade,
    heart,
    diamond,
    club
}
export class Card{
    numberCard: number;
    shape :Shape ;
    
    constructor(numberCard: number, shape: Shape)
    {
        this.numberCard= numberCard;
        this.shape= shape;
    }
 
    public getValue() :number{
        return(this.numberCard);
    }
    public GetShape() :Shape{
        return(this.shape);
    }
    public ToString(): string{
        return("the Number is "+this.numberCard+" \n his shape is "+this.shape);
    }
    
}
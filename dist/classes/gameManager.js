"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.startGame = exports.winnerComputer = exports.game = exports.winnerplayer = exports.lotteryNum = exports.dillCard = void 0;
const random_1 = require("../helper/random");
const functionHelpers_1 = require("../helper/functionHelpers");
function dillCard(player, diller) {
    console.log("player.getSum()", player.getSum());
    if (player.getSum() > 21) {
        return (false);
    }
    const crd = diller.getCard();
    console.log("card", crd);
    player.add(crd);
    console.log("your sum is " + player.getSum());
    return (true);
}
exports.dillCard = dillCard;
function lotteryNum(min, max) {
    return (Math.floor(Math.random() * (max - min + 1) + min));
}
exports.lotteryNum = lotteryNum;
function winnerplayer(players, randomNumOfRound) {
    for (let i = 0; i < players.length; i++) {
        if (players[i].getSum() >= lotteryNum(randomNumOfRound, 21)) {
            players[i].winner++;
        }
    }
}
exports.winnerplayer = winnerplayer;
function game(player, diller) {
    if (!dillCard(player, diller)) {
        console.log("you lose");
        return false;
    }
    return (true);
}
exports.game = game;
function winnerComputer(computerPlayers, randomNumOfRound) {
    let tempRandom;
    let numberwin = (0, random_1.randomNum)(randomNumOfRound);
    for (let i = 0; i < computerPlayers.length; i++) {
        tempRandom = (0, random_1.randomNum)(randomNumOfRound);
        if (tempRandom >= numberwin) {
            computerPlayers[i]++;
        }
    }
    return (computerPlayers);
}
exports.winnerComputer = winnerComputer;
function startGame(randomNumOfRound) {
    let question = "";
    if (randomNumOfRound == 15)
        return ((0, functionHelpers_1.CheckAnswer)("do you want to play"));
    if (randomNumOfRound > 15 && randomNumOfRound < 20) {
        return ((0, functionHelpers_1.CheckAnswer)("do you want to play another game?"));
    }
    if (randomNumOfRound == 20)
        return ((0, functionHelpers_1.CheckAnswer)("is you last turn do you want to stop?"));
    return (false);
}
exports.startGame = startGame;
//# sourceMappingURL=gameManager.js.map
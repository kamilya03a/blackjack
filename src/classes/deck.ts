import { ShuffleDesk } from "../helper/random";
import { Card } from "./card";
import { Shape } from "./card";
export class Deck {
    cards: Card[];
    size: number=52;
    lastCard: number;
    
    constructor()
    {
        this.cards = new Array(this.size);
        this.lastCard=0;
        Object.keys(Shape).forEach(shape => {

            for (let j:number=1; j < 14; j++) {
                if(j > 10) {
                    const card = new Card(10, shape as unknown as Shape);
                    this.add(card);
                }
                else {
                    const card = new Card(j, shape as unknown as Shape) 
                    this.add(card);
                }
            }
        })   
        this.cards= ShuffleDesk(this.cards); 
    }

    public isEmpty():boolean{
        if(this.lastCard>0)
            return true;
        return false;
    }

    private add(card: Card): void{
            this.cards[this.lastCard] = (card);
            this.lastCard++;
    }

    public getCard():Card{
    //בהנחה שיש קלפים בחפיסה
    let tempCard: Card= this.cards[this.lastCard-1];
    this.lastCard--;
        return(tempCard);
    }

    public getAmountOfCards():number{
        return(this.size);
    }

    public ToString (): string{
       let print:string="";
        for(let i:number=0; i<this.cards.length;i++)
        {
            print+=this.cards[i].ToString();
        }
        return(print);
    }
}
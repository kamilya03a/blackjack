"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const functionHelpers_1 = require("./helper/functionHelpers");
const deck_1 = require("./classes/deck");
const gameManager_1 = require("./classes/gameManager");
const ReadLineSync = __importStar(require("readline-sync"));
let players = (0, functionHelpers_1.buildPlayers)();
let length = ReadLineSync.questionInt("how many computer players do you want to play?");
let computerPlayers = new Array(length);
let sumPlayers = (0, functionHelpers_1.CopyPlayers)(players);
let randomNumOfRound = 15;
let diller = new deck_1.Deck();
let place;
while ((0, gameManager_1.startGame)(randomNumOfRound)) {
    while (players.length != 0 && (0, functionHelpers_1.CheckCardsEnough)(players.length, diller.getAmountOfCards())) {
        for (let i = 0; i < players.length; i++) {
            if (((0, functionHelpers_1.CheckAnswer)(`Your sum is ${players[i].getSum()} do you want to take a card?`))) {
                if (!((0, gameManager_1.game)(players[i], diller))) {
                    sumPlayers = (0, functionHelpers_1.getOut)(sumPlayers, players[i]);
                    players = (0, functionHelpers_1.getOut)(players, players[i]);
                }
                else {
                    place = sumPlayers.findIndex((player) => {
                        return (player.playerId == players[i].playerId);
                    });
                    sumPlayers[place].setSum(players[i].getSum());
                }
            }
            else {
                players = (0, functionHelpers_1.getOut)(players, players[i]);
            }
        }
    }
    if (sumPlayers.length == 0) {
        if (computerPlayers.length != 0) {
            computerPlayers = (0, gameManager_1.winnerComputer)(computerPlayers, randomNumOfRound);
        }
    }
    else {
        (0, gameManager_1.winnerplayer)(players, randomNumOfRound);
        (0, gameManager_1.winnerComputer)(computerPlayers, randomNumOfRound);
        randomNumOfRound++;
        players = (0, functionHelpers_1.CopyPlayers)(sumPlayers);
        diller = new deck_1.Deck();
        (0, functionHelpers_1.resetSum)(players);
    }
}
(0, functionHelpers_1.printWinnerPlayers)(sumPlayers);
(0, functionHelpers_1.printWinnerComputersPlayers)(computerPlayers);
//# sourceMappingURL=program.js.map
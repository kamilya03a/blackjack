import { Card } from "../classes/card";

export function ShuffleDesk(cards: Card[]) : Card[]
{
    let currentIndex = cards.length;
    let randomIndex;
    let temp;
    // While there remain elements to shuffle.
    while (currentIndex != 0) {
  
      // Pick a remaining element.
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;
  
      // And swap it with the current element.
      temp= cards[currentIndex];
      cards[currentIndex]= cards[randomIndex];
      cards[randomIndex]= temp;
      // [cards[currentIndex], cards[randomIndex]] = 
      // [cards[randomIndex], cards[currentIndex]];
    }
    return cards;
};

export function randomNum(min:number): number
{ 
        return (Math.floor(Math.random() * (21 - min + 1) + min));
}


import { Deck } from "./deck";
import { Player } from "./player";
import { randomNum } from "../helper/random";
import { CheckAnswer, CopyPlayers } from "../helper/functionHelpers";

export function dillCard(player: Player,diller: Deck):boolean{
    console.log("player.getSum()", player.getSum())
    if(player.getSum()> 21)
    {
        return (false);
    }
    const crd = diller.getCard()
    console.log("card", crd)
    player.add(crd)
    console.log("your sum is " + player.getSum());
    return (true);
}  

export function lotteryNum(min:number, max: number): number
{ 
        return (Math.floor(Math.random() * (max - min + 1) + min));
}

export function winnerplayer(players: Player[], randomNumOfRound:number): void
{
    for(let i: number=0; i< players.length;i++)
    {
        if(players[i].getSum()>=lotteryNum(randomNumOfRound,21))
        {
            players[i].winner++;
        }
    }
}

export function game(player: Player, diller:Deck): boolean{    
    if(!dillCard(player, diller))
        {
            console.log("you lose");
            return false;
        }
    return(true);        
}

export function winnerComputer(computerPlayers: number[],randomNumOfRound: number):number[]
{
    let tempRandom: number;
    let numberwin= randomNum(randomNumOfRound);
    for (let i :number=0; i<computerPlayers.length; i++)
    {
        tempRandom=randomNum(randomNumOfRound);
        if(tempRandom>= numberwin)
        {
            computerPlayers[i]++;
        }
    }
    return(computerPlayers);
}

export function startGame(randomNumOfRound: number): boolean
{
    let question:string =""
    if(randomNumOfRound==15)
      return (CheckAnswer("do you want to play"));
    if(randomNumOfRound>15 && randomNumOfRound<20)
    {
        return(CheckAnswer("do you want to play another game?"));
    }
    if(randomNumOfRound==20)
        return(CheckAnswer("is you last turn do you want to stop?"))
    return (false);
}
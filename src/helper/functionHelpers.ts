import { Player } from "../classes/player";
import * as ReadLineSync from "readline-sync";
import { randomNum } from "./random";
import { Deck } from "../classes/deck";

export function buildPlayers(): Player[]{
    let players: number= ReadLineSync.questionInt("how many players do you want to game with?");
    if(players==0)
        players=1;
    let array: Player[]=[];
    for(let i=0;i<players;i++)
    {
        array.push(new Player(i));
    }

    return(array);
}

export function CopyPlayers(players: Player[]):Player[]
{
   let tempPlayers:Player[]= [];

    for(let i: number=0; i<players.length;i++)
        tempPlayers.push(players[i]);

    return tempPlayers;    
}

export function buildComputerPlayers(randomNumOfRound: number, _max: number): number[]{
    let players: number= ReadLineSync.questionInt("how many players do you want to game with?");
    let array:number[]=[];
    for(let i=0;i<players-1;i++)
    {
        array[i]=0;
        array[i]= randomNum(randomNum(randomNumOfRound));
    }
    return(array);
}

export function getOut(arr:Player[], player: Player):Player[]
{
    let index = arr.indexOf(player);
    if(index!== -1)
        arr.splice(index,1)
    return arr;
}

export function CheckCardsEnough(numOfPlayers:number, deck:number):boolean
{
    return (deck>=numOfPlayers);
}

export function CheckAnswer(question: string):boolean
{
        if(ReadLineSync.keyInYN(question)){
            return true;
        }
    return false;
}

export function printWinnerPlayers(players: Player[]): void
{
    players.sort(function(a:Player,b:Player){
        return(b.winner-a.winner);
    })
    console.log("The player winners in order are")
    for(let i: number=0; i< players.length; i++)
    {
        console.log("player number"+players[i].getPlayerId()+"with "+players[i].winner+" winning");
    }
}

export function printWinnerComputersPlayers(computerPlayers: number[]): void
{
    computerPlayers.sort(function(a:number,b:number){
        return(b-a);
    })
    console.log("The player winners in order are")
    for(let i: number=0; i< computerPlayers.length; i++)
    {
        console.log("player number "+i+"with "+computerPlayers[i]+" winning");
    }
}

export function resetSum(array: Player[]):void
{
    for (let index: number = 0; index< array.length; index++) 
        array[index].setSum(0);
}
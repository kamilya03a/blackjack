"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Player = void 0;
const ReadLineSync = __importStar(require("readline-sync"));
class Player {
    constructor(index) {
        this.winner = 0;
        this.sum = 0;
        this.lastIndex = 0;
        this.cards = [];
        this.playerId = index;
    }
    setSum(newSum) {
        this.sum = newSum;
    }
    add(card) {
        this.cards[this.lastIndex] = card;
        this.lastIndex++;
        if (card.getValue() == 1) {
            this.asCardValue(card);
        }
        else
            this.sum += card.getValue();
    }
    GetWinner() {
        return (this.winner);
    }
    getSum() {
        return (this.sum);
    }
    getPlayerId() {
        return (this.playerId);
    }
    asCardValue(card) {
        if (!ReadLineSync.keyInYN("do you want to change the card to 11?")) {
            this.sum += card.getValue();
        }
        else
            this.sum += 11;
    }
}
exports.Player = Player;
//# sourceMappingURL=player.js.map
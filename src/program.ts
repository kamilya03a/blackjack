import { Player } from "./classes/player";
import { buildPlayers, CheckAnswer, CheckCardsEnough, CopyPlayers, getOut, printWinnerComputersPlayers, printWinnerPlayers, resetSum } from "./helper/functionHelpers";
import { Deck } from "./classes/deck";
import { game, startGame, winnerComputer, winnerplayer } from "./classes/gameManager";
import * as ReadLineSync from "readline-sync";

let players: Player[]= buildPlayers() ;
let length: number = ReadLineSync.questionInt("how many computer players do you want to play?");
let computerPlayers: number[]= new Array(length);
let sumPlayers:Player[] = CopyPlayers(players);
let randomNumOfRound: number=15;
let diller: Deck = new Deck (); 
let place: number;
while(startGame(randomNumOfRound))
{
    while(players.length!=0 && CheckCardsEnough(players.length, diller.getAmountOfCards()))
    {
        for (let i:number = 0; i < players.length; i++) 
        {
            if((CheckAnswer(`Your sum is ${players[i].getSum()} do you want to take a card?`)))
            {
                if(!(game(players[i],diller)))
                    {
                    sumPlayers= getOut(sumPlayers,players[i])
                    players = getOut(players,players[i]);
                    }
                else
                    {
                  place = sumPlayers.findIndex((player)=>{
                    return (player.playerId==players[i].playerId)
                    })
                 sumPlayers[place].setSum(players[i].getSum());
                    }
            }
            else
            {
                players = getOut(players,players[i]);
            }
        }
    } 
    if(sumPlayers.length==0)
    {
        if(computerPlayers.length!=0)
        {   
        computerPlayers= winnerComputer(computerPlayers,randomNumOfRound);
        }
    }  
    else
    {
        winnerplayer(players,randomNumOfRound); 
        winnerComputer(computerPlayers, randomNumOfRound);
        randomNumOfRound++;
        players= CopyPlayers(sumPlayers);
        diller = new Deck (); 
        resetSum(players);
    }
}
printWinnerPlayers(sumPlayers);
printWinnerComputersPlayers(computerPlayers);
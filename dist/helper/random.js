"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.randomNum = exports.ShuffleDesk = void 0;
function ShuffleDesk(cards) {
    let currentIndex = cards.length;
    let randomIndex;
    let temp;
    // While there remain elements to shuffle.
    while (currentIndex != 0) {
        // Pick a remaining element.
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;
        // And swap it with the current element.
        temp = cards[currentIndex];
        cards[currentIndex] = cards[randomIndex];
        cards[randomIndex] = temp;
        // [cards[currentIndex], cards[randomIndex]] = 
        // [cards[randomIndex], cards[currentIndex]];
    }
    return cards;
}
exports.ShuffleDesk = ShuffleDesk;
;
function randomNum(min) {
    return (Math.floor(Math.random() * (21 - min + 1) + min));
}
exports.randomNum = randomNum;
//# sourceMappingURL=random.js.map